import { createFormat, createItems, withScroll } from '@ytoune/virtualized'
import type { RenderItem, Sizes, Sticky } from '@ytoune/virtualized'
import { createMemo, createSignal, For, onCleanup, onMount } from 'solid-js'
import html from 'solid-js/html'

const unit = <T>(v: T) => v

export type GridProps = Readonly<{
  rowSizes: Sizes
  colSizes: Sizes
  renderItem: RenderItem<ReturnType<typeof html>>
  sticky: Sticky
}>
export const Grid = (props: GridProps) => {
  // eslint-disable-next-line prefer-const
  let wrap: HTMLDivElement | null = null
  const { init, onScroll, subscribe } = withScroll({
    divRef: () => wrap,
    set: f => {
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      setScroll(f)
    },
  })
  const [scroll, setScroll] = createSignal(init)
  const unsubscribe = subscribe()
  onMount(onScroll)
  onCleanup(unsubscribe)
  const format = createMemo(() =>
    createFormat({
      colSizes: props.colSizes,
      rowSizes: props.rowSizes,
    }),
  )
  const items = createMemo(() =>
    createItems(format(), scroll(), props.sticky, props.renderItem),
  )
  return html`
    <div
      ref=${(e: HTMLDivElement) => (wrap = e)}
      style=${() => format().outerStyle}
      onScroll=${(_: unknown) => {
        onScroll()
      }}
    >
      <div
        style=${() => {
          const f = format().innerStyle
          return {
            width: f.width,
            height: f.height,
            display: f.display,
            'grid-template': f.gridTemplate,
          }
        }}
      >
        <${For} each=${items}>${unit}<//>
      </div>
    </div>
  `
}
