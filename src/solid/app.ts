import { createSignal } from 'solid-js'
import html from 'solid-js/html'

import type { Mode, State } from '../store'
import { list, reduce } from '../store'

import { Table } from './table'

export const App = () => {
  const [state, setState] = createSignal<State>(list[0])
  const dispatch = (mode: Mode) => {
    setState(s => reduce(s, mode))
  }
  return html`
    <div style=${{ margin: 0, padding: 0, width: '100%', height: '100%' }}>
      <div
        style=${{
          margin: 0,
          padding: 0,
          width: '100%',
          height: '24px',
          'font-size': '16px',
          'line-height': '24px',
        }}
      >
        ${list.map(
          item => html`
            <button
              key=${item.mode}
              disabled=${() => item.mode === state().mode}
              onClick=${(_: unknown) => {
                dispatch(item.mode)
              }}
            >
              ${item.mode}
            </button>
          `,
        )}
      </div>
      <div
        style=${{
          margin: 0,
          padding: 0,
          width: '100%',
          height: 'calc(100% - 24px)',
        }}
      >
        <${Table} state=${state} />
      </div>
    </div>
  `
}
