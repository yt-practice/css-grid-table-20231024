import { main as mountWithPreact } from './preact/main'
import { main as mountWithReact } from './react/main'
import { main as mountWithSolid } from './solid/main'

document.body.innerHTML = ''

const createElement = (tag = 'div', parent = document.body) => {
  const div = document.createElement(tag)
  parent.append(div)
  return div
}

const tools = createElement('div')
tools.style.height = '1.5em'
const preview = createElement('div')
preview.style.height = 'calc(100% - 1.5em)'

const fns = new Set<() => void>()
const reset = () => {
  for (const f of fns) {
    fns.delete(f)
    f()
  }
  preview.innerHTML = ''
}

for (const [label, fn] of [
  [
    'preact',
    () => {
      reset()
      fns.add(mountWithPreact(preview))
    },
  ],
  [
    'react',
    () => {
      reset()
      fns.add(mountWithReact(preview))
    },
  ],
  [
    'solid',
    () => {
      reset()
      fns.add(mountWithSolid(preview))
    },
  ],
] as const) {
  const btn = createElement('button', tools) as HTMLButtonElement
  btn.innerText = label
  btn.addEventListener('click', e => {
    e.preventDefault()
    fn()
    tools.querySelectorAll('button').forEach(e => {
      e.disabled = false
    })
    btn.disabled = true
  })
}
