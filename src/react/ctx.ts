import { createContext, useContext } from 'react'

import type { State } from '../store'
import { list } from '../store'

const ctx = createContext<State>(list[0])
export const useApp = () => useContext(ctx)

export const { Provider } = ctx
