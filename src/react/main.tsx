/** @jsx h */

/* eslint-disable import/no-deprecated */
import { createElement as h } from 'react'
import { createRoot } from 'react-dom/client'

import { App } from './app'
import { FontSize } from './font-size'

export const main = (el: HTMLElement) => {
  const root = createRoot(el)
  root.render(
    <FontSize>
      <App />
    </FontSize>,
  )
  return () => {
    root.unmount()
  }
}
