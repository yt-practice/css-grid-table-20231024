/** @jsx h */

/* eslint-disable import/no-deprecated */
import { h, render } from 'preact'

import { App } from './app'
import { FontSize } from './font-size'

export const main = (el: HTMLElement) => {
  render(
    <FontSize>
      <App />
    </FontSize>,
    el,
  )
  return () => {
    render(null, el)
  }
}
