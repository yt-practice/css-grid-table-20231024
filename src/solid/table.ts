import type { CellStyle, RenderItem } from '@ytoune/virtualized'
import html from 'solid-js/html'

import { chars } from '../chars'
import type { State } from '../store'

import { useFontSize } from './font-size'
import { Grid } from './grid'

const useRenderItem = (
  fontSize: number,
): RenderItem<ReturnType<typeof html>> => {
  const span = (
    text: string | null,
    s: CellStyle,
    b: `#${string}` = '#fff',
  ) => html`
    <div
      style=${{
        'line-height': `${fontSize * 1.5}px`,
        'text-align': 'end',
        color: '#000',
        background: b,
        ...s,
        'grid-area': s.gridArea,
      }}
      key=${s.gridArea}
    >
      ${text}
    </div>
  `
  return (r, c, s) => {
    const back = (r + c) % 2 ? '#ccc' : '#fff'
    if (r && c) return span(chars[(r + c - 2) % chars.length]!, s, back)
    if (r) return span(`r${r}`, s, back)
    if (c) return span(`c${c}`, s, back)
    return span(null, s, back)
  }
}

type Props = { state: State }
export const Table = (props: Props) => {
  const fontSize = useFontSize()
  return html`
    <${Grid}
      rowSizes=${() => props.state.rows(fontSize())}
      colSizes=${() => props.state.cols(fontSize())}
      renderItem=${() => useRenderItem(fontSize())}
      sticky=${() => props.state.sticky}
    />
  `
}
