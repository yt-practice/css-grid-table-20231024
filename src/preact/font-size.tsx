/** @jsx h */
import { h } from 'preact'
import type { ReactNode } from 'preact/compat'
import {
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'preact/compat'

const getSize = (el?: Element) => {
  try {
    const { fontSize } = getComputedStyle(el || document.body)
    const m = fontSize.match(/^(\d+)px$/)
    return +(m?.[1] || 16)
  } catch {
    return 16
  }
}
const size = createContext(getSize())
const { Provider } = size
const style = { width: '100%', height: '100%' } as const
export const FontSize = ({ children }: { children: ReactNode }) => {
  const div = useRef<HTMLDivElement>(null)
  const [s, set] = useState(null as null | number)
  useEffect(() => {
    if (div.current) set(getSize(div.current))
  }, [div, set])
  return (
    <div ref={div} style={style}>
      {null !== s ? <Provider value={s}>{children}</Provider> : null}
    </div>
  )
}

export const useFontSize = (): number => useContext(size)
