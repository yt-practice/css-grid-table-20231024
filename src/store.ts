import type { Sizes, Sticky } from '@ytoune/virtualized'

const pos = (f: (fontSize: number) => Sizes) => f
const st = <M extends string>(
  s: Readonly<{
    mode: M
    cols: (fontSize: number) => Sizes
    rows: (fontSize: number) => Sizes
    sticky: Sticky
  }>,
) => s

const rows = (n: number) => pos(s => ({ length: n + 1, size: s * 1.5 }))

const sized = <C extends number, R extends number>(cols: C, rows: R) =>
  ({
    mode: `${cols}x${rows}`,
    cols: pos(s => ({ length: cols + 1, size: s * 4 })),
    rows: pos(s => ({ length: rows + 1, size: s * 1.5 })),
    sticky: { r: 0, c: 3 },
  }) as const

export const list = [
  sized(5, 50),
  sized(16, 500),
  sized(2500, 10000),
  sized(25_000, 1_000_000),
  sized(50, 100),
  st({
    mode: '[80,40*50]x350',
    cols: () => [80, ...Array<number>(50).fill(40)],
    rows: rows(350),
    sticky: { r: 2, c: 0 },
  }),
  st({
    mode: '[80,120,160,200,240,280]x350',
    cols: () => [80, 120, 160, 200, 240, 280],
    rows: rows(350),
    sticky: { r: 2, c: 0 },
  }),
  st({
    mode: '[80,40,60,80,100,120,140,160,180]x650',
    cols: () => [80, 40, 60, 80, 100, 120, 140, 160, 180],
    rows: rows(650),
    sticky: { r: 0, c: 0 },
  }),
  st({
    mode: '[80,40,60,80,100,120,140,160,180]x150000',
    cols: () => [80, 40, 60, 80, 100, 120, 140, 160, 180],
    rows: rows(150_000),
    sticky: { r: 0, c: 0 },
  }),
] as const

export type State = (typeof list)[number]
export type Mode = State['mode']
export const reduce = (state: State, mode: Mode): State =>
  (state.mode !== mode && list.find(m => m.mode === mode)) || state
