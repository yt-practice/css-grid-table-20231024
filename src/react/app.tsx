/** @jsx h */
import { createElement as h, useReducer } from 'react'

import { list, reduce } from '../store'

import { Provider } from './ctx'
import { Table } from './table'

export const App = () => {
  const [state, dispatch] = useReducer(reduce, list[0])
  return (
    <Provider value={state}>
      <div style={{ margin: 0, padding: 0, width: '100%', height: '100%' }}>
        <div
          style={{
            margin: 0,
            padding: 0,
            width: '100%',
            height: '24px',
            fontSize: '16px',
            lineHeight: '24px',
          }}
        >
          {list.map(item => (
            <button
              key={item.mode}
              disabled={item.mode === state.mode}
              onClick={() => {
                dispatch(item.mode)
              }}
            >
              {item.mode}
            </button>
          ))}
        </div>
        <div
          style={{
            margin: 0,
            padding: 0,
            width: '100%',
            height: 'calc(100% - 24px)',
          }}
        >
          <Table />
        </div>
      </div>
    </Provider>
  )
}
