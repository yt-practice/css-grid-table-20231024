import { createContext, createSignal, onMount, useContext } from 'solid-js'
import html from 'solid-js/html'

const getSize = (el?: Element) => {
  try {
    const { fontSize } = getComputedStyle(el || document.body)
    const m = fontSize.match(/^(\d+)px$/)
    return +(m?.[1] || 16)
  } catch {
    return 16
  }
}
const size = createContext(() => getSize())
export const FontSize = (props: { children: Node }) => {
  // eslint-disable-next-line prefer-const
  let div: HTMLDivElement | null = null
  const [s, set] = createSignal(null as null | number)
  onMount(() => {
    if (div) set(getSize(div))
  })
  const content = () => {
    const n = s()
    const c = props.children
    return null !== n ? c : null
  }
  const value = () => s() || getSize()
  return html`
    <div
      ref=${(e: HTMLDivElement) => (div = e)}
      style="width: 100%; height: 100%"
    >
      <${size.Provider} value=${() => value}>${content}<//>
    </div>
  `
}

export const useFontSize = (): (() => number) => useContext(size)
