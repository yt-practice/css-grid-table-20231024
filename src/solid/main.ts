/* eslint-disable import/no-deprecated */
import html from 'solid-js/html'
import { render } from 'solid-js/web'

import { App } from './app'
import { FontSize } from './font-size'

export const main = (el: HTMLElement) => {
  const rm = render(
    () => html`
      <${FontSize}><${App} /><//>
    `,
    el,
  )

  return () => {
    rm()
  }
}
