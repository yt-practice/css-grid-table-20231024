/** @jsx h */
import type { CellStyle, RenderItem } from '@ytoune/virtualized'
import { h } from 'preact'
import { useMemo } from 'preact/compat'

import { chars } from '../chars'

import { useApp } from './ctx'
import { useFontSize } from './font-size'
import { Grid } from './grid'

const useRenderItem = (fontSize: number) =>
  useMemo<RenderItem<JSX.Element>>(() => {
    const span = (
      text: string | null,
      s: CellStyle,
      b: `#${string}` = '#fff',
    ) => (
      <div
        style={{
          lineHeight: `${fontSize * 1.5}px`,
          textAlign: 'end',
          color: '#000',
          background: b,
          ...s,
        }}
        key={s.gridArea}
      >
        {text}
      </div>
    )
    return (r, c, s) => {
      const back = (r + c) % 2 ? '#ccc' : '#fff'
      if (r && c) return span(chars[(r + c - 2) % chars.length]!, s, back)
      if (r) return span(`r${r}`, s, back)
      if (c) return span(`c${c}`, s, back)
      return span(null, s, back)
    }
  }, [fontSize])

export const Table = () => {
  const { rows, cols, sticky } = useApp()
  const fontSize = useFontSize()
  const renderItem = useRenderItem(fontSize)
  return (
    <Grid
      rowSizes={rows(fontSize)}
      colSizes={cols(fontSize)}
      renderItem={renderItem}
      sticky={sticky}
    />
  )
}
