/** @jsx h */
import { createFormat, createItems, withScroll } from '@ytoune/virtualized'
import type { RenderItem, Sizes, Sticky } from '@ytoune/virtualized'
import {
  createElement as h,
  memo,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'

const emptyArr = [] as const

export type GridProps = Readonly<{
  rowSizes: Sizes
  colSizes: Sizes
  renderItem: RenderItem<JSX.Element>
  sticky: Sticky
}>
export const Grid = memo(
  ({ colSizes, rowSizes, renderItem, sticky }: GridProps) => {
    const wrap = useRef<HTMLDivElement>(null)
    const sc = useRef(null as null | ReturnType<typeof withScroll>)
    sc.current ||= withScroll({
      divRef: () => wrap.current,
      set: f => {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        setScroll(f)
      },
    })
    const { init, onScroll, subscribe } = sc.current
    const [scroll, setScroll] = useState(init)
    useEffect(subscribe, emptyArr)
    const format = useMemo(
      () => createFormat({ colSizes, rowSizes }),
      [colSizes, rowSizes],
    )
    const items = useMemo(
      () => createItems(format, scroll, sticky, renderItem),
      [format, scroll, sticky, renderItem],
    )
    return (
      <div ref={wrap} style={format.outerStyle} onScroll={onScroll}>
        <div style={format.innerStyle}>{items}</div>
      </div>
    )
  },
  (p, n) =>
    p.colSizes === n.colSizes &&
    p.rowSizes === n.rowSizes &&
    p.renderItem === n.renderItem &&
    p.sticky === n.sticky,
)
